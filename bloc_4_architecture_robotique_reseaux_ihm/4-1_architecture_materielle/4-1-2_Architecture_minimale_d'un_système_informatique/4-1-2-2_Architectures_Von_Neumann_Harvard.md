## Architecture minimale d'un système informatique: Introduction. Du transistor au processeur

L'objectif de ce premier chapitre ou séquence est de faire une présentation générale du rôle et des fonctionnalités principales d'un système d'exploitation.

1. Introduction. Du transistor au processeur (2 videos)
2. **Du transistor au processeur (fin) - Architectures Von Neumann / Harvard**
3. Le langage de la CPU, l’assembleur
4. Du langage haut niveau à l’assembleur

[![Vidéo 3 B4-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S2-V3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S2-V3.mp4)

[![Vidéo 4 B4-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S2-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S2-V4.mp4)

## Présentation

[Support de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/bloc_4_architecture_robotique_reseaux_ihm/4-1_architecture_materielle/4-1-2_Architecture_minimale_d'un_syst%C3%A8me_informatique/4-1-2_Texte_Archi_Mini.md)

