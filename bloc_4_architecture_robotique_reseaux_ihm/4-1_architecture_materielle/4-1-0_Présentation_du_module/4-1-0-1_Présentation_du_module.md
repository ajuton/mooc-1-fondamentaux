## Présentation du module : Architecture matérielle

[![Vidéo 1 B4-M1-S0 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-objectifs.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-objectifs.mp4)

## Présentation
[Support de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/bloc_4_architecture_robotique_reseaux_ihm/4-1_architecture_materielle/4-1-0_Pr%C3%A9sentation_du_module/4-1-0-2_Texte_Pr%C3%A9sentation.md) 

## Objectifs du module

## Prérequis
Aucun prérequis n'est nécessaire pour suivre ce module

## Temps d'apprentissage

## Présentation enseignant
