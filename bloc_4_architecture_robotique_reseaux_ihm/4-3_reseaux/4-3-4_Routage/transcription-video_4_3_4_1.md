# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.4.1 : Le routage statique

**[00:00:05]**

Nous allons commencer le chapitre 4, qui porte sur le routage. 

L'objectif de ce chapitre est de comprendre les mécanismes de routage des paquets IP à travers les réseaux locaux et Internet. Ainsi, nous verrons d'abord les mécanismes de routage statique et routage dynamique qui seront assimilés au moyen des modules de travaux pratiques sur Wireshark et Cisco Packet Tracer.

Le routage répond à la question comment assurer la communication entre les réseaux. En effet, pour pouvoir communiquer sur Internet avec des serveurs situés à l'autre bout du monde, l'information peut passer par plusieurs réseaux interconnectés. La liaison entre ces réseaux se fait au moyen d'un matériel spécifique appelé le routeur. 
Dans cette vidéo, nous verrons ce que c'est qu'un routeur, la table de routage et les algorithmes de routage statique.

Le routeur est un matériel de couche 3 qui relie plusieurs réseaux. Il a donc une interface dans chacun des réseaux auxquels il est interconnecté. C'est donc tout simplement une machine qui a plusieurs interfaces, c'est-à-dire plusieurs cartes de réseaux, chacune reliée à un réseau différent. Son rôle va être d'aiguiller les paquets reçus entre les différents réseaux. Ainsi, un ordinateur ayant deux cartes réseau pourra être un routeur si elle est configurée de façon à accepter de relayer des paquets qui ne sont pas pour elle, alors qu'une simple machine ne les considérera pas. Le routeur est la porte de sortie et d'entrée des trames du réseau. Ainsi, lorsqu'une trame n'est pas destinée à une machine du sous réseau, elle est transmise au routeur qui la transmet à l'autre sous réseau. Les grands opérateurs sont tous reliés entre eux par les réseaux de type Tier 1.

**[00:01:47]**

Il s'agit des architectures similaires à la figure à droite, où tous les sous réseaux peuvent communiquer entre eux au travers de routeurs, dans le cloud ou dans le fog computing assurant la communication entre eux. 

Une table de routage est une structure de données utilisée par un routeur ou un ordinateur en réseau et qui associe des préfixes à des moyens d'acheminer les trames vers leur destination. C'est un élément central du routage IP. 
Le principe est d'avoir d'un côté la liste des réseaux que l'on veut joindre et de l'autre côté, la liste des routeurs à qui envoyer les paquets pour joindre ces réseaux. On appelle aussi ces routeurs des passerelles, car ils servent de passerelle entre deux réseaux. Considérant le réseau de la figure à droite, la table de routage du routeur en aura tout d'abord les adresses des réseaux auxquels il est directement connecté. Les routes statiques que l'administrateur aura ajoutées manuellement et les roues dynamiques qui l'aura découvert au moyen d'un protocole approprié que nous verrons dans la partie 3.
Pour un nœud tel que le nœud d'adresses 192.168.0.1, il a l'adresse du son sous réseau avec son adresse comme passerelle, car il a un accès direct aux nœuds de son sous réseau et la route par défaut. La route par défaut indiquera comment acheminer le trafic qui ne correspond à aucune entrée dans la table de routage. En l'absence d'une route par défaut, le routeur éliminera un paquet dont la destination n'est pas connue. Quand plusieurs routes sont possibles, la route la plus spécifique sera utilisée, c'est-à-dire celle qui aura le préfixe le plus long. Si plusieurs routes avaient le même préfixe, existent,

**[00:03:26]**

l'arbitrage a lieu en fonction du type de route. Les routes directement connectées auront la priorité sur les autres. Les routes statiques et dynamiques seront départagés par une distance administrative paramétrable. Si tous les paramètres sont égaux, le routeur pourra distribuer le trafic entre ces routes ou n’en utiliser qu'une seule.
La distance administrative est une valeur comprise entre 0 et 255 qui permet de savoir quelle route choisir quand le routeur indique plusieurs routes possibles. La valeur la plus prioritaire est de 0 et le niveau de priorité décroit jusqu'à la valeur 255. 

Enfin, le routage statique. On sait déjà ce que c'est qu'une table de routage. Le routage statique permet de rajouter des informations dans la table de routage de façon manuelle. Il s'agit d'indiquer pour un réseau d'adresse et de masque donné, quelle est l'adresse du routeur par lequel il faut transiter pour atteindre le réseau. Dans une architecture comme celle-ci, il s'agira de dire au routeur 1, par exemple, que pour envoyer des paquets au réseau 20.20.20.0/24, il doit les envoyer à l'interface du routeur R2 située dans le réseau 1.1.1.0/24 et pareil pour le routeur R2. Cela se fait sur un routeur Cisco avec la commande IP route suivie de l'adresse du réseau, son masque, l'adresse de l'interface connectée et le nom du réseau destinataire. 
Après, au niveau de chaque routeur, on peut voir la table de routage avec la commande show ip route. On peut voir après le C, les réseaux directement connectés à une interface du routeur et après le S, les réseaux ajoutés de façon statique.

