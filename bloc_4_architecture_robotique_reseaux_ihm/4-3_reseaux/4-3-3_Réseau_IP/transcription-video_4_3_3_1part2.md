# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.3.1 : IPV4 part 2

**[00:00:01]**

Le codage des adresses IP sur 32 bits a engendré peu à peu une pénurie d'adresses. En effet, 2 puissance 32 adresses, soit plus de 4 milliards d'adresses, paraissait un très grand nombre à l'époque de sa création. L'augmentation intense du nombre d'utilisateurs et du nombre d'appareils connectés pour chacun d'entre eux, ainsi que les plages d'adresses trop grandes qui ont été attribuées au début des adresses IPv4 font qu'il n'en existe plus de disponibles depuis novembre 2019. Différents remèdes sont mis en place depuis des années pour ralentir le phénomène, comme par exemple la traduction d'adresses le NAT dont je viens, que je viens d'évoquer et qu'on verra plus tard, ou encore la réattribution de plages d'adresses non utilisées. Ces solutions ne sont pas suffisantes et un passage se fait progressivement vers les adresses IPv6 utilisant un d'adressage sur 128 bits, ce qui permet d'avoir 340*10³⁶ adresses, un nombre très conséquent. L'ipv6 fera l'objet d'une prochaine vidéo. 
Les adresses IP sont donc utilisées au niveau de la couche réseau au sein du protocole IP. Le protocole IP définit un entête placé avant les données et comportant les informations suivantes. Nous avons au début la version dans laquelle on mettra IPv4 ou IPV6, par exemple, la longueur de l'entête, le type de service qui va être utilisé dans le message, la longueur totale du message et d'autres informations. Les informations les plus importantes sont évidemment l'adresse IP source et l'adresse IP de destination dont nous venons de parler. Également, la durée de vie du paquet qui définira le nombre de routeurs que peut passer ce paquet avant d'être périmé et donc d'être supprimé par les routeurs.

**[00:01:34]**

Également, une somme de contrôle d'entête de 16 bits qui vérifie que l'entête n'a pas été altéré durant la transmission. Et enfin, les données sont donc encapsulées dans ce paquet IP. Ce paquet IP, c'est à dire l'entête dont on vient de parler ainsi que les données associées, va être encapsulé dans une trame Ethernet. Ça veut dire que ce paquet IP est encapsulé dans la trame Ethernet que nous avons vu dans une précédente vidéo. C'est un peu comme une enveloppe dans une enveloppe. Nous avons l'enveloppe Ethernet qu'on a vu précédemment et l'enveloppe IP que nous venons de voir avec les adresses IP et les différentes informations dont nous venons de parler. 

Pour conclure, nous avons parlé dans cette vidéo de la couche réseau. Nous avons notamment évoqué les adresses IP composées de 4 octets, une partie réseau et une partie hête. Ce sont des adresses que l'on appelle logiques qui sont attribuées automatiquement ou manuellement par l'utilisateur. 
Nous avons vu qu'il existait trois plages d'adresses privées, une pour chaque classe, la classe A, la classe B et la classe C. 
Nous avons ensuite parlé de masque de sous réseau. Le masque de sous réseau sert à séparer la partie réseau de la partie hôte. C'est une série de 1, puis de 0 que l'on peut noter sous deux formes la forme décimale et la forme CIDR. Nous avons également parlé de l'adresse réseau et de l'adresse de broadcast. On appelle aussi l'adresse de diffusion.

