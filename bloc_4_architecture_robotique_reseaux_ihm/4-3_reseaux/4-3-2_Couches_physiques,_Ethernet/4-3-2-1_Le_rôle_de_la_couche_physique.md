# Couches Physiques, Ethernet 1/3: le rôle de la couche physique

1. **La couche physique**
2. Le protocole Ethernet
3. Relier des hôtes : le hub et le switch

[![Vidéo 1 B4-M3-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S2_video1_CouchePhy.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S2_video1_CouchePhy.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
