# Introduction aux réseaux 1/5: Différents réseaux pour différents usages

1. **Différents réseaux pour différents usages**
2. Notions générales sur les réseaux
3. Des transmissions essentiellement numériques
4. Introduction à Ethernet/TCP/IP
5. le modèle OSI

- **Anthony Juton** nous présente le sommaire de cette introduction aux réseaux et nous dresse, dans cette video, un panorama de différents réseaux associés à la diversité des applications

[![Vidéo 1 B4-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S1-Video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S1-Video1.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
