# Sécurité 1/5: sécurité du réseau local - Firewall / Proxy / DMZ

1. **Firewall / Proxy / DMZ**
2. Chiffrement
3. Les protocoles SSH/SSL
4. VPN
5. VLAN

[![Vidéo 1 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video1_SecuResLocal.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video1_SecuResLocal.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
