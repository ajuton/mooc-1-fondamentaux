# Sécurité 2/5: Chiffrement

1. Firewall / Proxy / DMZ
2. **Chiffrement**
3. Les protocoles SSH/SSL
4. VPN
5. VLAN

[![Vidéo 2part1 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video2p1-Chiffrement.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video2p1-Chiffrement.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 2part2 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video2p2-Chiffrement.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video2p2-Chiffrement.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
