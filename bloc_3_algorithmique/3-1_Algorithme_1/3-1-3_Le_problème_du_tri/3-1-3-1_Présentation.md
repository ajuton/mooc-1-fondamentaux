## Le problème du tri :Présentation

Dans cette séquence de vidéos, nous allons aborder un problème qui est un problème classique qu’on appelle le problème du tri. Le problème du tri, c'est tout simplement réorganiser des données de façon à pouvoir être plus efficace pour les traiter par la suite... Nous aurons une première partie qui va justifier pourquoi on essaye de trier et une deuxième partie sur 2 tris, le tri par insertion et le tri par sélection qui sont des tris que j'appelle itératifs c'est à dire dans lequel petit à petit on construit la liste ou le tableau triés : on a des incréments de 1, c'est à dire que, petit à petit, on rajoute un élément et ça augmente la qualité de ce qu’on est en train de regarder jusqu’à arriver à la fin quand on aura traiter les n éléments.

1. **Présentation**
2. Recherche d'un élément, recherche dichotomique (itératif récursif, (diviser pour régner)
3. Le problème du tri : contraintes, propriétés attendues
4. Le tri par sélection (preuve, complexité)
5. Le tri par insertion (preuve, complexité)
6. Autres tris (bulle,...)

[![Vidéo 1 B3-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S3-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S3-video1.mp4)


