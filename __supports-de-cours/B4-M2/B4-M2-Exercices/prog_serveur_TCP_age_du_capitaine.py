#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 00:41:45 2021

@author: ajuton
"""

#l'age du capitaine

import socket #importation de la bibliothèque python socket
import threading
import random

thread = 0

#fonction serveur lancée pour chaque tâche correspondant à chaque connexion
def instanceServeur (socket_client, adress_client):
  global thread
  thread += 1
  num_thread = thread
  age = random.randint(20,90)
  print("Thread " + str(num_thread) +" Client connecté. Adresse " + adress_client[0] + " port : " + str(adress_client[1]))
  nb_tentative = 0
  print("Thread " + str(num_thread) +" L'âge du capitaine est " + str(age))
  socket_client.send("Quel est l'âge du capitaine ? les propositions sont des entiers".encode("utf-8"))
  proposition_client = 0
  while (proposition_client != age) : #tant que le client n'a pas trouvé
    #réception puis affichage du message du client
    donnees_recues_du_client = socket_client.recv(4096)
    nb_tentative+=1
    print("Thread " + str(num_thread) + " reçu : " + donnees_recues_du_client.decode("utf-8")) 
    proposition_client=int(donnees_recues_du_client)
    if(proposition_client<age) :
      socket_client.send("Le capitaine est plus vieux".encode("utf-8"))
    elif(proposition_client>age) :
      socket_client.send("Le capitaine est plus jeune".encode("utf-8"))
    else :
      socket_client.send(("Bravo, vous avez trouvé après "+str(nb_tentative)+" tentative(s)").encode("utf-8"))
    
  #fermeture du socket
  print("Thread " + str(num_thread) +" Connexion fermée avec " + adress_client[0] + ":" + str(adress_client[1]))
  socket_client.close()


#création du socket du serveur    
adresse_socket = ("", 8082)  # all interfaces, port 8081
socket_serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket_serveur.bind(adresse_socket)
socket_serveur.listen()

while True:
  socket_cree_pour_client, adresse_client = socket_serveur.accept() #acceptation d'une demande de connexion
  #création d'une tâche pour la nouvelle connexion
  threading._start_new_thread(instanceServeur, (socket_cree_pour_client, adresse_client))
	
socket_serveur.close()




