#!/usr/bin/env python3

import os
import signal

def handler(sig,f):
    print('trop tard! \n')
    os._exit(0)

def main():
    signal.signal(signal.SIGALRM,handler)
    signal.alarm(5)
    nb = input('entrer un nombre avant 5 sec. : ')
    print('Vous avez rentré ', nb)
    os._exit(0)

main()

