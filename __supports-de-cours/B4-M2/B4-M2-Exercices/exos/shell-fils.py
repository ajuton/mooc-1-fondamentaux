#!/usr/bin/env python3

import os
import time

def main():
    print('[main] mon pid est ', os.getpid())
    print('[main] mon père est ', os.getppid())

main()
