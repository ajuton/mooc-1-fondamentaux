#!/usr/bin/env python3

import os
import time

def main():
    print('[père] mon pid est ', os.getpid())
    newpid = os.fork()
    if newpid == 0:
        print('[fils] mon pid est ', os.getpid())
        for i in range(10):
            time.sleep(3)
            print('[fils] ', i)
        os._exit(22)
    else:
        print('[père] mon fils a comme pid ', newpid)
        for i in range(10):
            time.sleep(1)
            print(i)
        res = os.waitpid(newpid, 0)
        status = res[1]
        if os.WIFEXITED(status):
            print(  '[père] mon fils', res[0],
                    'a terminé normalement avec valeur :', os.WEXITSTATUS(status))
        else:
            print('[père] mon fils', res[0], 'a termine anormalement')
    os._exit(0)

main()
