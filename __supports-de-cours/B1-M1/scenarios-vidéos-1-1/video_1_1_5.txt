MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_5
durée : 4'

Titre :  Changements de base


Dans la vidéo précédente, nous avons vu comment représenter des nombres entiers et positifs dans une base arbitraire. Comment faire si le nombre n'est pas entier, c'est-à-dire s'il a des chiffres après la virgule ?

Considérons le nombre 6,625 que nous voulons exprimer en base 2. Cela revient à le décomposer en une somme de puissances de 2, et nous savons déjà que la partie entière, 6, vaut 4+2 et s'exprime donc 110 en base 2. 

Pour traiter le 0,625, nous allons le multiplier par 2 de manière répétée.

0,625 fois 2 donne 1,25. Ce résultat est plus grand que 1, ce qui nous indique que 0,625 est plus grand qu'1/2. Nous devons donc mettre à 1 le bit correspondant à 1/2.

Il nous reste alors à traiter la partie fractionnaire, c'est-à-dire 0,25. À nouveau, nous le multiplions par 2, ce qui donne 0,5. Le résultat est plus petit que 1, le bit suivant doit donc être mis à 0.

On multiplie à nouveau la partie fractionnaire par 2, et on obtient 1. Le dernier bit est donc à 1 également. Le processus s'arrête car la partie fractionnaire est maintenant nulle.




Dans cet exemple, la procédure de conversion en base 2 s'est terminée, mais ce ne sera pas toujours le cas. Certains nombres ont une représentation finie dans certaines bases mais pas dans d'autres. Par exemple, le nombre 1/3 s'écrit 0,1 en base 3, mais a une représentation infinie en base 10.

Quand on a une partie fractionnaire qui contient une même séquence de chiffres qui se répète infiniment souvent, on parle de de représentation infinie périodique, et on écrit une seule fois la période qu'on souligne.

Cela peut aussi avoir lieu en base 2. Considérons le nombre 0,54 en base 10. Appliquons la procédure vue précédemment. Après quatre étapes, nous constatons que la même partie fractionnaire 0,8 se répète. Cette fois-ci la procédure ne va donc pas terminer, mais va produire la même séquence de chiffres 1100 répétée infiniment souvent. 0,54 s'exprime donc de façon infinie périodique en base 2.

Rappelons aussi qu'il existe des nombres irrationnels comme pi, racine de deux ou la constante e. Ceux-ci n'admettent  ni une représentation finie, ni même une représentation infinie périodique, dans aucune base entière.

Enfin, notons qu'une représentation en base 2 d'un nombre avec une partie fractionnaire n'est pas une représentation binaire, car nous avons besoin de trois symboles: le 0, le 1 et la virgule ! Nous verrons plus tard comment éviter la virgule. Cela nous permettra de représenter les nombres fractionnaires dans une mémoire d'ordinateur à condition qu'ils ne nécessitent qu'un nombre fini de 0 et de 1.






