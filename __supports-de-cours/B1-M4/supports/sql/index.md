Cours de bases de données - Modèles et langages
===============================================

Contents:

::: {.toctree maxdepth="3"}
intro relationnel calcul alg sql conception schemas procedures
transactions etudecas annales
:::

Indices and tables
==================

-   `genindex`{.interpreted-text role="ref"}
-   `modindex`{.interpreted-text role="ref"}
-   `search`{.interpreted-text role="ref"}
