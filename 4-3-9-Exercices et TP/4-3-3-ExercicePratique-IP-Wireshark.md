# Exercice pratique : Mise en oeuvre de Wireshark, découverte de son réseau local

L'objectif de ce TP est de mettre en oeuvre les principaux outils réseaux à travers la découverte du réseau local du domicile. 

## La configuration de la machine
Les commandes permettant de connaître la configuration de la machine sont : 

* Sous Windows, depuis une console CMD (nommée _Invite de commande_ dans le menu Système Windows sous Windows 10) : `ipconfig /all`
<img src="image.png" alt="Menu Démarrer et Invite de commande" width="600px;">
<img src="image-1.png" alt="Exemple d'utilisation de ipconfig" width="600px;">

* Sous Linux : 	
    * `ip addr` ou `ip a` affiche les adresses de la machine
    * `ip route` affiche la table de routage ipv4 de la machine
	* `ip -6 route` indique la table de routage ipv6 de la machine
	* `nano /etc/resolv.conf` affiche les serveurs DNS de la machine

Relever la configuration de la machine utilisée, pour chaque carte réseau, (réseau wifi et réseau ethernet) :

* adresse IP v4 avec son masque de sous-réseau et au format CIDR (Classless Inter-Domain Routing) et la durée du bail,
* adresses IP v6 au format CIDR, et les durées de bail,
* passerelle par défaut,
* le serveur DNS de la machine. (le serveur DNS est la machine qui traduit les noms de domaines en adresse IP)

Indiquer auprès de qui sont référencées les ipv6 utilisées. On peut par exemple utiliser le  site web de l'association RIPE pour cela : https://stat.ripe.net/app/launchpad/ . Un autre site est https://findipv6.com/find/ 
Pour localiser une adresse ipv4, on peut utiliser RIPE ou le site suivant par exemple : https://mon-adresse-ip.fr/

Indiquer quelles sont les machines voisines dont la carte ethernet est connue, à l’aide de la commande `arp -a` sous windows et de la commande `ip neigh` sous linux.
Pour chercher toutes les machines du sous-réseau, on peut utiliser l'application nmap. Présente sur Linux (ou installable via `sudo apt install nmap`), elle est téléchargeable pour Windows (https://nmap.org/download.html). L'application angry_IP (https://angryip.org) permet également de scanner le réseau.
La commande Linux `sudo nmap -sn -PE -n 192.168.1.1-254` permet d'interroger les machines ayant une adresse de 192.168.1.1 à 192.168.1.254.
A l'aide du site https://macvendors.com/, retrouver la marque de certaines machines à partir de leur adresse MAC.


## La configuration de la box
Se connecter à la box internet (son adresse est celle de la passerelle obtenue ci-dessus, souvent 192.168.1.1 ou 192.168.1.254). Il faut sur certaines box passer en mode utilisateur avancé pour avoir les informations demandées ci-dessous.

### Le réseau local LAN
Faire une copie d’écran de la page ou des pages de configuration du réseau local.

* Quelles sont les IPv4 distribuées par la box ?
* A quelles machines correspondent-elles ? Lesquelles parmi ces machines sont des serveurs ?
* Réserver une IP pour un des serveurs ou pour le PC sinon. Cela se fait depuis le menu serveur DHCP normalement.

<img src="image-2.png" alt="Page web de configuration du serveur DHCP de la box" width="600px;">

### Le réseau WAN
Faire une copie d’écran de la page d’information sur la configuration wan (appelée aussi configuration Internet)

* Quelle est l’adresse IP v4 de la box ? (la page http://ip.lafibre.info/ peut être utile)
* Est-ce une adresse IP publique ou privée ? 
* A qui est-elle attribuée par l’ICANN ? (via  https://stat.ripe.net/app/launchpad/ )
* Quels sont les serveurs DNS de la box ? A qui sont attribuées ces adresses ?
* Quelle est l’adresse IP v6 de la box ? A qui est-elle attribuée ?

## Le routage Internet des paquets depuis le réseau local

Pour tracer le chemin d’un paquet, on utilise la commande __traceroute__ en indiquant sa destination. Les exemples sont données avec le serveur dns.google 8.8.8.8. 
L’exercice est à faire avec une autre machine publique et « lointaine », au choix.

* Windows :	`tracert dns.google` ou `tracert 8.8.8.8` par exemple
* Linux : `traceroute dns.google` ou `traceroute dns.google`

Pour faire un tracert en ipv6, on utilise :

* Windows :	tracert -6 dns.google
* Linux : 	traceroute6 dns.google

Certains routeurs ne répondent pas aux messages ICMP utilisés par traceroute, c'est pourquoi on obtient des * pour ces routeurs "muets".

### Chemin parcouru par un paquet

Faire une carte du chemin théorique suivi par le paquet. On peut pour cela utiliser le site https://ip-info.org donnant les adresses physiques des propriétaires d'une adresse IP.
Attention, il est écrit théorique car certaines IP peuvent être attribuées à une entreprise américaine et être physiquement en Europe, sur une réplication du serveur américain, par exemple.
Indiquer sur la carte les IP des routeurs traversés.

## Utilisation d'un analyseur de réseau logiciel

Il est possible d'observer les trames échangées par le PC et Internet grâce à un outil d'analyse de réseau, complètement logiciel. Il a juste besoin d'un accès à la carte réseau du PC.

### Fonctionnement de traceroute
Lancer wireshark et faire l’acquisition des échanges liés à la requête traceroute. Le filtre icmp ou icmpv6 est intéressant ici pour trier les paquets utiles à la question.
S’appuyer les requêtes et réponses pour expliquer le fonctionnement de traceroute.

