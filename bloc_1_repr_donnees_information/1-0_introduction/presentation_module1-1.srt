1
00:00:03,228 --> 00:00:04,755
Bonjour à toutes et à tous !

2
00:00:04,855 --> 00:00:05,976
Bienvenue dans cette section

3
00:00:06,076 --> 00:00:07,425
sur les types de données de base.

4
00:00:07,525 --> 00:00:09,320
Dans cette section, nous allons étudier

5
00:00:09,420 --> 00:00:10,576
la façon dont l'ordinateur

6
00:00:10,676 --> 00:00:12,043
représente les informations

7
00:00:12,143 --> 00:00:13,339
pour les manipuler.

8
00:00:14,162 --> 00:00:15,811
Nous savons toutes et tous

9
00:00:15,911 --> 00:00:17,647
que l'ordinateur représente l'information

10
00:00:17,747 --> 00:00:19,850
à l'aide de 0 et de 1.

11
00:00:19,950 --> 00:00:21,448
Nous allons développer cette idée

12
00:00:21,548 --> 00:00:23,623
et comprendre comment on représente

13
00:00:23,723 --> 00:00:25,103
différents types d'information

14
00:00:25,203 --> 00:00:26,644
comme des nombres,

15
00:00:26,744 --> 00:00:27,524
du texte,

16
00:00:27,624 --> 00:00:28,839
des images, du son

17
00:00:28,939 --> 00:00:29,915
ou même de la vidéo.

18
00:00:30,937 --> 00:00:32,515
Ces vidéos présentent la matière

19
00:00:32,615 --> 00:00:34,063
qui peut être donnée aux élèves.

20
00:00:34,163 --> 00:00:36,193
Mais le support écrit va plus en profondeur

21
00:00:36,293 --> 00:00:38,478
et présente de la matière supplémentaire

22
00:00:38,578 --> 00:00:40,085
pour approfondir le contenu.

23
00:00:40,623 --> 00:00:41,370
Vous êtes prêts ?

24
00:00:41,470 --> 00:00:42,593
Commençons tout de suite

25
00:00:42,693 --> 00:00:44,777
en étudiant la représentation binaire

26
00:00:44,877 --> 00:00:46,055
dans la première vidéo.

