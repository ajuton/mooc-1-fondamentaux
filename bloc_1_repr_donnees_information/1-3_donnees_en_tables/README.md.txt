# Sommaire B1-M3

## 1-3 Présentation du module : Objectifs du module

## 1-3-1 Introduction sur les données en tables

## 1-3-2 Recherches dans une table

## 1-3-3 Trier des données

## 1-3-4 Fusion de table

## 1-3-5 Sauvegarde des données

## 1-3-6 Jointure

## 1-3-7 Exercices : TP



