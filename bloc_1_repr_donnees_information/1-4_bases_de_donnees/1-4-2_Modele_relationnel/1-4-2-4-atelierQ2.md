## Le modèle relationnel. Atelier : une étude de cas 2/3

### Recherche d'anomalies

On peut mettre n'importe quoi dans cette relation. Par exemple on
pourrait y trouver le contenu de la table ci-dessous (on a simplifié le
nombre de colonnes).

animal |   nom   |    espèce |   gardien  |  salaire  |   classe  |   origine  |   emplacement
--- | --- | --- | --- | --- | --- | --- | --- 
10  | Zoé     |  Girafe |  Marcel  |  10 000 |   Poisson  | Afrique |   A
20  | Martin  |  Ours  |   Marcel |   9 000  |   Insecte  |  Europe |    B
30  | Martin  |  Girafe |  Jules  |   12 000  |  Poisson  | Afrique |  A
20  | Goupil  |  Renard |  Marcel  |  10 000 |   Oiseau  |  Europe  |  B
40  | Goupil  | Renard  | Jules |    12 000  |  Insecte  |  Asie   |   A


Maintenant:

> -   Q1. Citer (au moins) 5 anomalies qui rendent cette table incompatible
>     avec les dépendances fonctionnelles données précédemment.
>
> -   Q2. Citer (au moins) 2 redondances qui pourraient être évitées.
