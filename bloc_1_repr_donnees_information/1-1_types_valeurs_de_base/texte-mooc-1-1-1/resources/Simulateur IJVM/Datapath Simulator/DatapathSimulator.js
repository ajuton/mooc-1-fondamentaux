/*
 * Datapath simulator
 * 
 * Program by Meire Wouter, Master student at ULB
 * wmeire@ulb.ac.be
 */
///////////////////////////////////// Global variables /////////////////////////////////////

var MAR = "#MAR";
var MDR = "#MDR";
var PC = "#PC";
var MBR = "#MBR";
var SP = "#SP";
var LV = "#LV";
var CPP = "#CPP";
var TOS = "#TOS";
var OPC = "#OPC";
var H = "#H";
var Z = "#Z";
var N = "#N";

var nopID = "nop";
var iaddID = "iadd";
var bipushID = "bipush";
var gotoID = "goto";
var ifeqID = "ifeq";
var isubID = "isub";
var iandID = "iand";
var iorID = "ior";
var dupID = "dup";
var popID = "pop";
var swapID = "swap";
var iincID = "iinc";
var ifltID = "iflt";
var if_icmpeqID = "if_icmpeq";

var microprogram = new Array(
		["Main1", "PC = PC + 1; fetch; goto (IR)", "IR holds opcode; get next byte; dispatch"],
		["nop1", "goto Main1", "Do nothing"],
		["iadd1", "MAR = SP = SP - 1; rd", "Read in next-to-top word on stack"],
		["iadd2", "H = TOS", "H = top of stack"],
		["iadd3", "MDR = TOS = MDR + H; wr; goto Main1", "Add top two words; write to top of stack"],
		["isub1", "MAR = SP = SP - 1; rd", "Read in next-to-top word on stack"],
		["isub2", "H = TOS", "H = top of stack"],
		["isub3", "MDR = TOS = MDR - H; wr; goto Main1", "Do subtraction; write to top of stack"],
		["iand1", "MAR = SP = SP - 1; rd", "Read in next-to-top word on stack"],
		["iand2", "H = TOS", "H = top of stack"],
		["iand3", "MDR = TOS = MDR AND H; wr; goto Main1", "Do AND; write to new top of stack"],
		["ior1", "MAR = SP= SP - 1; rd", "Read in next-to-top word on stack"],
		["ior2", "H = TOS", "H = top of stack"],
		["ior3", "MDR = TOS = MDR OR H; wr; goto Main1", "Do OR; write to new top of stack"],
		["dup1", "MAR = SP = SP + 1", "Increment SP and copy to MAR"],
		["dup2", "MDR = TOS; wr; goto Main1", "Write new stack word"],
		["pop1", "MAR = SP = SP - 1; rd", "Read in next-to-top word on stack"],
		["pop2", "&nbsp", "Wait for new TOS to be read from memory"],
		["pop3", "TOS = MDR; goto Main1", "Copy new word to TOS"],
		["swap1", "MAR = SP - 1; rd", "Set MAR to SP - 1; read 2nd word from stack"],
		["swap2", "MAR = SP" , "Set MAR to top word"],
		["swap3", "H = MDR; wr", "Save TOS in H; write 2nd word to top of stack"],
		["swap4", "MDR = TOS", "Copy old TOS to MDR"],
		["swap5", "MAR = SP - 1; wr", "Set MAR to SP - 1; write as 2nd word on stack"],
		["swap6", "TOS = H; goto Main1", "Update TOS"],
		["bipush1", "SP = MAR = SP + 1" ,"IR = the byte to push onto stack"],
		["bipush2", "PC = PC + 1; fetch", "Increment PC, fetch next opcode"],
		["bipush3", "MDR = TOS = IR; wr; goto Main1", "Sign-extend constant and push on stack"],
		["iinc1", "H = LV", "IR contains index; Copy LV to H"],
		["iinc2", "MAR = IRU + H; rd", "Copy LV + index to MAR; Read variable"],
		["iinc3", "PC = PC + 1; fetch", "Fetch constant"],
		["iinc4", "H = MDR", "Copy variable to H"],
		["iinc5", "PC = PC + 1; fetch", "Fetch next opcode"],
		["iinc6", "MDR = IR + H; wr; gotoMain1", "Put sum in MDR; update variable"],
		["goto1", "OPC = PC - 1" , "Save address of opcode"],
		["goto2", "PC = PC + 1; fetch", "IR = 1st byte of offset; fetch 2nd byte"],
		["goto3", "H = IR << 8", "Shift and save signed first byte in H"],
		["goto4", "H = IRU OR H" , "H = 16-bit branch offset"],
		["goto5", "PC = OPC + H; fetch", "Add offset to OPC"],
		["goto6", "goto Main1", "Wait for fetch of next opcode"],
		["iflt1", "MAR = SP = SP - 1; rd", "Read in next-to-top word on stack"],
		["iflt2", "OPC = TOS", "Save TOS in OPC temporarily"],
		["iflt3", "TOS = MDR", "Put new top of stack in TOS"],
		["iflt4", "N = OPC; if (N) goto T; else goto F", "Branch on N bit"],
		["ifeq1", "MAR = SP = SP - 1; rd", "Read in next-to-top word of stack"],
		["ifeq2", "OPC = TOS", "Save TOS in OPC temporarily"],
		["ifeq3", "TOS = MDR", "Put new top of stack in TOS"],
		["ifeq4", "Z = OPC; if (Z) goto T; else goto F", "Branch on Z bit"],
		["if_icmpeq1", "MAR = SP = SP - 1; rd", "Read in next-to-top word of stack"],
		["if_icmpeq2", "MAR = SP = SP - 1", "Set MAR to read in new top-of-stack"],
		["if_icmpeq3", "H = MDR; rd", "Copy second stack word to H"],
		["if_icmpeq4", "OPC = TOS", "Save TOS in OPC temporarily"],
		["if_icmpeq5", "TOS = MDR", "Put new top of stack in TOS"],
		["if_icmpeq6", "Z = OPC - H; if (Z) goto T; else goto F", "If top 2 words are equal, goto T, else goto F"],
		["T", "OPC = PC - 1; goto goto2", "Same as goto1; needed for target address"],
		["F", "PC = PC + 1", "Skip first offset byte"],
		["F2", "PC = PC + 1; fetch", "PC now points to next opcode"],
		["F3", "goto Main1", "Wait for fetch of opcode"]
);

var examples = new Array(
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0x12"));
			instructionObjects.push(new Bipush("0x30"));
			instructionObjects.push(new Iadd());
			return instructionObjects;
		},
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0xa1"));
			instructionObjects.push(new Goto("0x5"));
			instructionObjects.push(new Bipush("0xb2"));
			instructionObjects.push(new Bipush("0xa0"));
			instructionObjects.push(new Iinc("0x2", "0x24"));
			return instructionObjects;
		},
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0x0"));
			instructionObjects.push(new Ifeq("0x5", instructionObjects));
			instructionObjects.push(new Bipush("0x1a"));
			instructionObjects.push(new Bipush("0x2b"));
			return instructionObjects;
		},
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0x31"));
			instructionObjects.push(new Ifeq("0x5", instructionObjects));
			instructionObjects.push(new Bipush("0x1a"));
			instructionObjects.push(new Bipush("0x2b"));
			return instructionObjects;
		},
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0x15"));
			instructionObjects.push(new Bipush("0x51"));
			instructionObjects.push(new Swap());
			instructionObjects.push(new Ior());
			return instructionObjects;
		},
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0xc3"));
			instructionObjects.push(new Dup());
			instructionObjects.push(new Pop());
			return instructionObjects;
		},
		function(){
			instructionObjects = new Array();
			instructionObjects.push(new Bipush("0xb1"));
			instructionObjects.push(new Bipush("0xb2"));
			instructionObjects.push(new Swap());
			instructionObjects.push(new Bipush("0xb1"));
			instructionObjects.push(new If_icmpeq("0x15", instructionObjects));
			return instructionObjects;
		}
);


///////////////////////////////////// Objects /////////////////////////////////////

function Microcode(instructionObjects){
	this.state = new MicrocodeState();
	this.instructionObjects = instructionObjects;
	this.generateMicrocodeInfoLines();
	this.generateMicrocodeHTMLTextLines();
	this.generateMicrocodeOperationsAndOperationIDS();
	this.generateNbrOfSteps();
}

function MicrocodeState(){
	this.stack = new Array("...");
	this.currentStep = 0;
	this.PCVal = 0;
	this.MARPos = 0;
	this.SPPos = 0;
	this.fetchCountdown = [0,0];
	this.readCountdown = [0,0];
	this.writeCountdown = [0,0];
	this.stepHighlightEnabled = false;
}

///////////////////////////////////// Microcode object /////////////////////////////////////
/////////////////////////////////////     methods      /////////////////////////////////////

///////////////////////////////////// Microcode object construction related /////////////////////////////////////

Microcode.prototype.generateMicrocodeInfoLines = function(){
	this.infoLines = new Array();

	for (var i=0; i < this.instructionObjects.length; ++i) {
		this.infoLines = this.infoLines.concat(this.instructionObjects[i].getInfoLines());
	}
};

Microcode.prototype.generateMicrocodeHTMLTextLines = function(){
	this.hTMLTextLines = new Array();

	var step = 0;
	var hTMLText = "";
	var length = this.infoLines.length;

	for (var i=0; i < length; ++i) {
		hTMLText += "<span class=displaySpacing>" + step.toString(16) + " : </span> ";
		for (var j=0; j < this.infoLines[i].length; ++j) {
			hTMLText += "<span class=displaySpacing>" + this.infoLines[i][j] + "</span> ";	
		}

		this.hTMLTextLines = this.hTMLTextLines.concat(hTMLText);
		hTMLText = "";
		step++;
	}
};

Microcode.prototype.generateMicrocodeOperationsAndOperationIDS = function(){
	this.operations = new Array();
	this.operationIDs = new Array();

	var length = this.instructionObjects.length;

	if(length != 0){
		this.operations = this.state.initialValues(this.instructionObjects[0].opcode);
		this.operationIDs = this.operationIDs.concat("");
		for (var i=0; i < length; ++i) {
			this.operations = this.operations.concat(main1Operations);
			this.operations = this.operations.concat(this.instructionObjects[i].operations);

			this.operationIDs = this.operationIDs.concat(main1OperationIDs);
			this.operationIDs = this.operationIDs.concat(this.instructionObjects[i].operationIDs);
		}
		this.operations = this.operations.concat(main1Operations);
		this.operationIDs = this.operationIDs.concat(main1OperationIDs);
	}
};

Microcode.prototype.generateNbrOfSteps = function(){
	this.nbrOfSteps = this.operations.length;
};


///////////////////////////////////// Microcode Object related /////////////////////////////////////

Microcode.prototype.initiateFetch = function(){
	if(this.state.PCVal >= this.infoLines.length){ // When PCVal reached the end of the microcode
		this.state.fetchCountdown = [1,"..."];
	} else
		this.state.fetchCountdown = [1,stringHexaWith0xToStringHexaWithout0x(this.infoLines[this.state.PCVal][0])];

	$("#Fetch_Image").show();
};

Microcode.prototype.initiateWrite = function(){
	this.state.writeCountdown = [2, $(MDR).val()];
};

Microcode.prototype.initiateRead = function(){
	if(this.state.MARPos >= this.state.stack.length){
		this.state.initialState();
		fillAllDisplays();
		throw "MAR went through the top !";
	}
	if(this.state.MARPos == 0)
		this.state.readCountdown = [2,"..."];
	else
		this.state.readCountdown = [2,this.state.stack[this.state.MARPos.toString(16)]];
};

Microcode.prototype.executeNextStep = function(){
	resetRegColors();

	var length = this.operations.length;

	if(this.state.currentStep < length) {
		if($(Z).val() != "...")
			$(Z).val("...");
		if($(N).val() != "...")
			$(N).val("...");
		this.operations[this.state.currentStep](this);

		this.state.stepHighlightEnabled = true;
		this.state.currentStep++;
	}

	$("#stepTracker").html(this.state.currentStep + "/" + this.nbrOfSteps);
	$("#instructionOperationStepTracker").html(this.state.currentStep + "/" + this.nbrOfSteps);
};

Microcode.prototype.handlePCJmp = function(){
	if(this.state.PCVal < this.infoLines.length){
		if(this.infoLines[this.state.PCVal].length < 2){ // every instruction has at least the opcode and the instruction name
			this.state.initialState();
			fillAllDisplays();
			throw "Not a suitable place to jump (not at the start of a new instruction)";
		}
		else {
			this.adaptToNewPCVal();
		}
	} else
		// The last step to be executed is Main1 (last in operations), but currentStep will be incremented once before the next nextOperation
		this.state.currentStep = microcode.nbrOfSteps - 2; 
};

Microcode.prototype.adaptToNewPCVal = function(){
	var currentStepCnt = 0;
	var objectCnt = 0;
	var tempPC = 0;

	while(tempPC < this.state.PCVal) {
		// currentStepCnt += main1 step + microinstruction steps
		currentStepCnt += (1 + (this.instructionObjects[objectCnt].operations.length));

		// "tempPC" should increment as much as the numbers of lines of the current microinstruction
		tempPC += ((this.instructionObjects[objectCnt].getInfoLines()).length);
		objectCnt++;
	}

	this.state.currentStep = currentStepCnt;
};


///////////////////////////////////// MicrocodeState object /////////////////////////////////////
/////////////////////////////////////        methods        /////////////////////////////////////

MicrocodeState.prototype.initialState = function(){
	resetRegColors();
	resetRegValues();

	this.currentStep = 0;
	this.PCVal = 0;
	this.SPPos = 0;
	this.MARPos = 0;
	this.fetchCountdown = [0,0];
	this.readCountdown = [0,0];
	this.writeCountdown = [0,0];
	this.stepHighlightEnabled = false;

	this.emptyStack();

	$("#stepTracker").html("0/" + microcode.nbrOfSteps);

	$("#Fetch_Image").hide();
	$("#Write_Image").hide();
	$("#Read_Image").hide();
};

MicrocodeState.prototype.emptyStack = function(){
	this.stack = new Array("...");
};

MicrocodeState.prototype.insertInStack = function(value, pos){
	if(pos != undefined)
		this.stack[pos] = value;
	else
		this.stack.push(value);
};

MicrocodeState.prototype.setMARVal = function(value){
	this.MARPos = value;
	this.setNewRegValAndNewPosInStackAndHighlight(MAR, this.MARPos);
};

MicrocodeState.prototype.updateMAR = function(modification){
	this.MARPos += modification;
	if(this.MARPos < 0){
		this.initialState();
		fillAllDisplays();
		throw "MAR went through the bottom !";
	}
	this.setNewRegValAndNewPosInStackAndHighlight(MAR, this.MARPos);
};

MicrocodeState.prototype.updateSP = function(modification){
	this.SPPos += modification;
	if(this.SPPos < 0){
		this.initialState();
		fillAllDisplays();
		throw "SP went through the bottom !";
	}
	this.setNewRegValAndNewPosInStackAndHighlight(SP, this.SPPos);
};

MicrocodeState.prototype.initialValues = function(instructionOpcode){
	return new Array(
			function(){
				setNewRegValAndHighlight(PC, "0");
				setNewRegValAndHighlight(MBR, stringHexaWith0xToStringHexaWithout0x(instructionOpcode));
			});
};

MicrocodeState.prototype.checkIfMemoryAccess = function(){
	this.checkIfFetch();
	this.checkIfWrite();
	this.checkIfRead();
};

MicrocodeState.prototype.checkIfFetch = function(){
	if(this.fetchCountdown[0] == 1) {
		this.fetchCountdown[0]--;

		$(MBR).val(this.fetchCountdown[1]);
		$(MBR).css("color", "blue");
		$(MBR).css("background-color", "#CCFFCC");

		$("#Fetch_Image").hide();
	}
};

MicrocodeState.prototype.checkIfWrite = function(){
	if(this.writeCountdown[0] == 2){
		this.writeCountdown[0]--;
		$("#Write_Image").show();
	}
	else if(this.writeCountdown[0] == 1) {
		this.writeCountdown[0]--;

		this.insertInStack(this.writeCountdown[1], this.MARPos);

		$("#Write_Image").hide();
	}
};

MicrocodeState.prototype.checkIfRead = function(){
	if(this.readCountdown[0] == 2){
		this.readCountdown[0]--;
		$("#Read_Image").show();

	} else if(this.readCountdown[0] == 1) {
		this.readCountdown[0]--;

		$(MDR).val(this.readCountdown[1]);
		$(MDR).css("color", "blue");
		$(MDR).css("background-color", "#CCFFCC");

		$("#Read_Image").hide();
	}
};

MicrocodeState.prototype.setNewRegValAndNewPosInStackAndHighlight = function(reg, pos){
	if (reg == MAR)
		this.MARPos = pos;
	else if (reg == SP)
		this.SPPos = pos;

	setNewRegValAndHighlight(reg,"n + " + pos.toString(16));
};

MicrocodeState.prototype.programClosing = function(){
	this.checkIfMemoryAccess();

	$("#Fetch_Image").show(); // main1 should always be the last instruction in this case
};


/////////////////////////////////////   Global functions /////////////////////////////////////
/////////////////////////////////////          &         /////////////////////////////////////
/////////////////////////////////////      microcode     /////////////////////////////////////

var microcode = new Microcode(new Array());


///////////////////////////////////// Instruction dependent /////////////////////////////////////

function instructionAddition(instructionID){
	var newInstructionObject = new Array();
	var info1 = $("#additionalInformationField_A").val();
	var info2 = $("#additionalInformationField_B").val();

	if (instructionID == iaddID) {
		newInstructionObject.push(new Iadd());
	} else if (instructionID == isubID) {
		newInstructionObject.push(new Isub());
	} else if (instructionID == iandID) {
		newInstructionObject.push(new Iand());
	} else if (instructionID == iorID) {
		newInstructionObject.push(new Ior());
	} else if (instructionID == dupID) {
		newInstructionObject.push(new Dup());
	} else if (instructionID == popID) {
		newInstructionObject.push(new Pop());
	} else if (instructionID == swapID) {
		newInstructionObject.push(new Swap());
	} else if (instructionID == nopID) {
		newInstructionObject.push(new Nop());
	} else if (instructionID == bipushID) {
		if(checkIfValidByte(info1))
			newInstructionObject.push(new Bipush("0x" + info1));
	} else if (instructionID == iincID) {
		if(checkIfValidByte(info1) && checkIfValidByte(info2))
			newInstructionObject.push(new Iinc("0x" + info1, "0x" + info2));
	} else if (instructionID == gotoID) {
		if(checkIfValid2Bytes(info1))
			newInstructionObject.push(new Goto("0x" + info1));
	} else if (instructionID == ifeqID) {
		if(checkIfValid2Bytes(info1))
			newInstructionObject.push(new Ifeq(("0x" + info1), microcode.instructionObjects));
	} else if (instructionID == ifltID) {
		if(checkIfValid2Bytes(info1))
			newInstructionObject.push(new Iflt(("0x" + info1), microcode.instructionObjects));
	} else if (instructionID == if_icmpeqID) {
		if(checkIfValid2Bytes(info1))
			newInstructionObject.push(new If_icmpeq(("0x" + info1), microcode.instructionObjects));
	}

	if(newInstructionObject.length > 0)
		microcode = new Microcode(microcode.instructionObjects.concat(newInstructionObject));
}


///////////////////////////////////// (Re)Initializers /////////////////////////////////////

function resetRegColors() {
	$(MAR).css("color", "black");
	$(MAR).css("background-color", "white");

	$(MDR).css("color", "black");
	$(MDR).css("background-color", "white");

	$(PC).css("color", "black");
	$(PC).css("background-color", "white");

	$(MBR).css("color", "black");
	$(MBR).css("background-color", "white");

	$(SP).css("color", "black");
	$(SP).css("background-color", "white");

	$(LV).css("color", "black");
	$(LV).css("background-color", "white");

	$(CPP).css("color", "black");
	$(CPP).css("background-color", "white");

	$(TOS).css("color", "black");
	$(TOS).css("background-color", "white");

	$(OPC).css("color", "black");
	$(OPC).css("background-color", "white");

	$(H).css("color", "black");
	$(H).css("background-color", "white");

	$(Z).css("color", "black");
	$(Z).css("background-color", "white");

	$(N).css("color", "black");
	$(N).css("background-color", "white");
}

function resetRegValues() {
	$(MAR).val("n");
	$(MDR).val(0);
	$(PC).val(0);
	$(MBR).val(0);
	$(SP).val("n");
	$(LV).val(0);
	$(CPP).val("...");
	$(TOS).val("...");
	$(OPC).val(0);
	$(H).val(0);
	$(Z).val("...");
	$(N).val("...");
}

function resetThumbnailParameters() {
	$("#Home_Page").hide();
	$("#Microcode").hide();
	$("#Microprogram").hide();
	$("#Stack").hide();
	$("#microcodeForInstructions").hide();
	$("#Toolbox").hide();

	$("#microcodeCreationButtons").hide();
	$("#operationBrowsingButtons").hide();
	$("#instructionOperationBrowsingButtons").hide();

	$("#Microcode_Text").html("");
	$("#Microprogram_Text").html("");
	$("#microcodeForInstructions_Text").html("");

	$("#Fetch_Image").hide();
	$("#Write_Image").hide();
	$("#Read_Image").hide();

	$("#Home_Page_Spacing").hide();

	$("#Microprogram_Text").css("height", "10em");
	$("#Mic1_Wrapper").show();
	$("#Right_Wrapper").css("width", "63%");
	$("#Right_Wrapper").css("float", "right");

	microcode = new Microcode(new Array());
}

function setMicrointructionParameters(){
	$("#Microprogram").show();
	$("#Stack").show();
	$("#microcodeForInstructions").show();

	$("#instructionOperationBrowsingButtons").show();
}

function setupExample(exampleID){
	resetThumbnailParameters();
	$("#Microcode").show();
	$("#Microprogram").show();
	$("#Stack").show();

	$("#operationBrowsingButtons").show();

	microcode = new Microcode(examples[exampleID]());

	fillAllDisplays();

	$("#stepTracker").html("0/" + microcode.nbrOfSteps);
}

///////////////////////////////////// Displays /////////////////////////////////////

function fillAllDisplays(){
	var microcodeHTMLTextAndFocus = generateMicrocodeHTMLTextAndFocus();
	fillMicrocodeDisplay(microcodeHTMLTextAndFocus);
	fillMicroprogramDisplay();
	fillStackDisplay();
	fillMicrocodeDisplayForInstructions(microcodeHTMLTextAndFocus);
}

function fillMicrocodeDisplay(microcodeHTMLTextAndFocus){
	$("#Microcode_Text").html(microcodeHTMLTextAndFocus[0]);

	var lineHeight = parseInt($("#Microcode_Text").css('line-height'));
	$("#Microcode_Text").scrollTop(lineHeight * microcodeHTMLTextAndFocus[1]);
}

function fillMicrocodeDisplayForInstructions(microcodeHTMLTextAndFocus){
	$("#microcodeForInstructions_Text").html(microcodeHTMLTextAndFocus[0]);

	var lineHeight = parseInt($("#microcodeForInstructions_Text").css('line-height'));
	$("#microcodeForInstructions_Text").scrollTop(lineHeight * microcodeHTMLTextAndFocus[1]);
}

function generateMicrocodeHTMLTextAndFocus(){
	var microcodeHTMLText = "";
	var lineFocusNbr = 0;
	var length = microcode.hTMLTextLines.length;
	var difference = microcode.state.PCVal - length;

	for (var i=0; i < length; ++i) {
		if((microcode.state.PCVal == i) && microcode.state.stepHighlightEnabled){
			microcodeHTMLText += "<span style=\"color:blue\">" + microcode.hTMLTextLines[i] +
			"<span class=displaySpacing> <- PC </span> </span> </br>";

			lineFocusNbr = i;
		} else {
			microcodeHTMLText += microcode.hTMLTextLines[i] + "</br>";
		}
	}

	if(difference >= 0 && microcode.state.PCVal > 0) {
		microcodeHTMLText += "<span style=\"color:blue\">" +
		"<span class=displaySpacing> " + length.toString(16) + " + " + difference + " :</span> <span class=displaySpacing> ... </span> " +
		"<span class=displaySpacing> <- PC </span> </span> </br>";

		lineFocusNbr = length;
	} else {
		microcodeHTMLText += "<span class=displaySpacing> " + length.toString(16) + " :</span> <span class=displaySpacing> ... </span> </br>";
	}

	return [microcodeHTMLText, lineFocusNbr];
}

function fillMicroprogramDisplay() {
	var lineFocusNbr = 0;
	var microprogramHTMLText = "";

	for(var i=0; i < microprogram.length; ++i) {
		if(microcode.state.currentStep < microcode.operationIDs.length &&
				microcode.operationIDs[microcode.state.currentStep] == microprogram[i][0] && microcode.state.stepHighlightEnabled){
			microprogramHTMLText += "<span style=\"color:blue\"> <span class=displaySpacing> " +
			microprogram[i][0] + " :</span> <span class=displaySpacingL>" + microprogram[i][1] + "</span>" +
			"<span class=displaySpacing> <- MPC </span> </span> </br>";

			lineFocusNbr = i;
		} else {
			microprogramHTMLText += "<span class=displaySpacing> " + microprogram[i][0] + " :</span>" +
			"<span class=displaySpacingL>" + microprogram[i][1] + "</span></br>";
		}
	}

	$("#Microprogram_Text").html(microprogramHTMLText);

	var lineHeight = parseInt($("#Microprogram_Text").css('line-height'));
	$("#Microprogram_Text").scrollTop(lineHeight * lineFocusNbr);
}

function fillStackDisplay() {
	var stackText = "";
	var stackLength = microcode.state.stack.length;

	stackText += regHigherThenStack(stackLength);

	for (var i=(stackLength-1); i >= 0 ; --i){
		var textIntro = "";
		var textAppendage = "</br>";
		if (i == microcode.state.MARPos && i == microcode.state.SPPos && microcode.state.stepHighlightEnabled){
			textIntro = "<span style=\"color:blue\">";
			if(i == 0)
				textAppendage = "<span class=displaySpacing> <- SP, MAR, LV </span></span>" + textAppendage;
			else
				textAppendage = "<span class=displaySpacing> <- SP, MAR </span></span>" + textAppendage;
		} else if (i == microcode.state.MARPos && microcode.state.stepHighlightEnabled){
			if(i == 0)
				textAppendage = "<span class=displaySpacing> <- MAR, LV </span>" + textAppendage;
			else
				textAppendage = "<span class=displaySpacing> <- MAR</span>" + textAppendage;
		} else if (i == microcode.state.SPPos && microcode.state.stepHighlightEnabled){
			textIntro = "<span style=\"color:blue\">";
			if(i == 0)
				textAppendage = "<span class=displaySpacing> <- SP, LV </span></span>" + textAppendage;
			else
				textAppendage = "<span class=displaySpacing> <- SP </span></span>" + textAppendage;
		}else if (i == 0 && microcode.state.stepHighlightEnabled){
			textAppendage = "<span class=displaySpacing> <- LV </span></span>" + textAppendage;
		}

		stackText += textIntro + "<span class=displaySpacing> n + " + i.toString(16) + " :</span>" +
		"<span class=displaySpacing>" + microcode.state.stack[i] + "</span>" +
		textAppendage;
	}

	$("#Stack_Text").html(stackText);
	$("#Stack_Text").scrollTop();
}

//MAR and/or SP can point to a value exeeding by 1 the stack
function regHigherThenStack(stackLength){
	var stackText = "";

	if(stackLength == microcode.state.MARPos && stackLength == microcode.state.SPPos){
		stackText += "<span style=\"color:blue\"> <span class=displaySpacing> n + " + stackLength.toString(16) + " :</span>" +
		"<span class=displaySpacing> _ </span> <span class=displaySpacing> <- MAR, SP </span> </span> </br>";
	} else if(stackLength == microcode.state.MARPos){
		stackText += "<span class=displaySpacing> n + " + stackLength.toString(16) + " :</span>" +
		"<span class=displaySpacing> _ </span> <span class=displaySpacing> <- MAR </span> </br>";
	} else if (stackLength == microcode.state.SPPos){
		stackText += "<span style=\"color:blue\"> <span class=displaySpacing> n + " + stackLength.toString(16) + " :</span>" +
		"<span class=displaySpacing> _ </span> <span class=displaySpacing> <- SP </span> </span> </br>";
	}

	return stackText;
}

function displayMicroprogram(){
	var hTMLText = "<span class=largerBoldText><span class=displaySpacing>Label</span>" +
			"<span class=displaySpacingL>Operations</span>" +
			"<span class=displaySpacingL>Comments</span></span></br>";
	for(var i=0; i < microprogram.length; ++i){
		hTMLText += "<span class=displaySpacing>" + microprogram[i][0] + "</span>";
		for(var j=1; j < microprogram[i].length; ++j){
			hTMLText += "<span class=displaySpacingL>" + microprogram[i][j] + "</span>";
		}
		hTMLText += "</br>";
	}

	$(Microprogram_Text).html(hTMLText);
}

///////////////////////////////////// clicks //////////////////////////////////////////////

$(document).ready(function(){
	$("#Home").click(function(){
		resetThumbnailParameters();
		$("#Home_Page").show();
		$("#Home_Page_Spacing").show();
	});


	$("#MicroprogramThumbnail").click(function(){
		resetThumbnailParameters();

		displayMicroprogram();

		$("#Mic1_Wrapper").hide();
		$("#Right_Wrapper").css("width", "90%");
		$("#Right_Wrapper").css("float", "none");
		$("#Microprogram_Text").css("height", "45em");
		$("#Microprogram").show();
	});

	$("#microcode_Builder").click(function(){
		resetThumbnailParameters();
		$("#Microcode").show();
		$("#Toolbox").show();
		$("#microcodeCreationButtons").show();
		$("#invalidInput").hide();

		fillMicrocodeDisplay(generateMicrocodeHTMLTextAndFocus());
	});

	$("#Example1").click(function(){
		setupExample(0);
	});

	$("#Example2").click(function(){
		setupExample(1);
	});

	$("#Example3").click(function(){
		setupExample(2);
	});

	$("#Example4").click(function(){
		setupExample(3);
	});

	$("#Example5").click(function(){
		setupExample(4);
	});

	$("#Example6").click(function(){
		setupExample(5);
	});

	$("#Example7").click(function(){
		setupExample(6);
	});

	$("#addInstruction").on("click", function(){
		$("#invalidInput").hide();

		instructionAddition($("#instruction-options").val());
		fillAllDisplays();
	});

	$("#closeMicrocodeBuilder").click(function(){
		$("#microcodeCreationButtons").hide();
		$("#operationBrowsingButtons").show();
		$("#Stack").show();
		$("#Microprogram").show();
		$("#Toolbox").hide();

		$("#stepTracker").html("0/" + microcode.nbrOfSteps);
	});

	$("#previousOperation").on("click", function(){
		var newCurrentStep = microcode.state.currentStep - 1;

		microcode.state.initialState();
		$("#stepTracker").html("0/" + microcode.nbrOfSteps);

		while(microcode.state.currentStep < newCurrentStep){
			var currentStepSave = microcode.state.currentStep;

			microcode.executeNextStep();

			if(microcode.state.currentStep > newCurrentStep){ // possible because of jumps
				microcode.state.initialState();

				while(microcode.state.currentStep < currentStepSave){
					microcode.executeNextStep();
				}
				break;
			}
		}
		fillAllDisplays();
	});

	$("#nextOperation").on("click", function(){
		microcode.executeNextStep();
		fillAllDisplays();
	});

	$("#resetProgress").on("click", function(){
		microcode.state.initialState();
		fillAllDisplays();
	});

	$("#eraseInstruction").click(function(){
		var newInstructionObjects = microcode.instructionObjects;
		newInstructionObjects.pop();
		microcode = new Microcode(newInstructionObjects);
		fillAllDisplays();
	});

	$("#eraseAll").click(function(){
		microcode = new Microcode(new Array());
		fillAllDisplays();
	});

	$("#instruction-options").change(function(){
		var instruction = $("#instruction-options").val();

		if (instruction == iaddID || instruction == isubID || instruction == iandID || instruction == iorID || 
				instruction == dupID || instruction == popID || instruction == swapID || instruction == nopID) {
			$("#additionalInformationField_A").attr("disabled",true);
			$("#additionalInformationField_B").attr("disabled",true);
		}
		if (instruction == bipushID || instruction == gotoID || instruction == ifeqID ||
				instruction == ifltID ||  instruction == if_icmpeqID) {
			$("#additionalInformationField_A").attr("disabled",false);
			$("#additionalInformationField_B").attr("disabled",true);
		}
		if (instruction == iincID) {
			$("#additionalInformationField_A").attr("disabled",false);
			$("#additionalInformationField_B").attr("disabled",false);
		}
	});

	$("#previousInstructionOperation").on("click", function(){
		var newCurrentStep = microcode.state.currentStep - 1;

		if(microcode.instructionObjects[0].ID == ifeqID || microcode.instructionObjects[0].ID == ifltID)
			microcode.instructionObjects[0].setupInstruction(microcode, microcode.instructionObjects[0].number);
		else if(microcode.instructionObjects[0].ID == if_icmpeqID)
			microcode.instructionObjects[0].setupInstruction(microcode,
					[microcode.instructionObjects[0].number1, microcode.instructionObjects[0].number2]);
		else
			microcode.instructionObjects[0].setupInstruction(microcode);

		while(microcode.state.currentStep < newCurrentStep){
			microcode.executeNextStep();
		}
		fillAllDisplays();
	});

	$("#nextInstructionOperation").on("click", function(){
		microcode.executeNextStep();
		fillAllDisplays();
	});
});


///////////////////////////////////// Other /////////////////////////////////////

function stringHexaWith0xToStringHexaWithout0x(stringHexa){
	return parseInt(stringHexa).toString(16);
}

function setNewRegValAndHighlight(reg, val) {
	$(reg).val(val);
	$(reg).css("color", "blue");
	$(reg).css("background-color", "#CCCCFF");
}

function checkIfValidByte(input){
	var pattern = /^[a-f0-9]{1,2}$/i;

	if(! pattern.test(input)){
		$("#invalidInput").text("Invalid byte");
		$("#invalidInput").show();
		return false;
	}
	return true; 
}

function checkIfValid2Bytes(input){
	var pattern = /^[a-f0-9]{1,4}$/i;

	if(! pattern.test(input)){
		$("#invalidInput").text("Invalid double byte");
		$("#invalidInput").show();
		return false;
	}
	return true; 
}

function twosComplementNegateNumber(number){
	number = ~number + 1;
	return number;
}

function trunc(number){
	if(number > 0xff){
		number = number - 0x100;
	}
	return number;
}

function testZ(number){
	if(number == 0){
		setNewRegValAndHighlight(Z, 1);
	} else
		setNewRegValAndHighlight(Z, 0);
}

function testN(number){
	if((number & 0x80) > 0)
		setNewRegValAndHighlight(N, 1);
	else
		setNewRegValAndHighlight(N, 0);
}

