Maintenant que nous avons
étudié des techniques pour représenter et manipuler des \emph{nombres}
en binaire, intéressons-nous à d'autres types d'informations. Nous
commencerons par le \emph{texte}, c'est-à-dire des séquences de
caractères. L'idée générale de la représentation (en binaire) des
caractères consiste à faire correspondre à chaque caractère un et un
seul nombre naturel, et à représenter le caractère par la
représentation binaire de ce naturel. Comme il n'existe pas de manière
universelle et naturelle d'associer un nombre à chaque caractère, il
faut fixer une table, qui fait correspondre chaque caractère à un
nombre.



\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{images/Chap2/Emile_Baudot.jpg}
  \caption*{\'Emile \textsc{Baudot}}
\end{figure}
\paragraph{Codes historiques: \'Emile \textsc{Baudot} et les
  téléscripteurs}
Le besoin de représenter et transmettre de l'information de manière
mécanique est une préoccupation qui a reçu une première réponse lors
de l'introduction du télégraphe, où le fameux code Morse était utilisé
pour représenter les lettres par une série de traits et de points. Au
dix-neuvième siècle, un français, Jean-Maurice \'Emile
\textsc{Baudot}\footnote{Ingénieur français, né le 11 septembre 1845,
  mort le 28 mars 1903.} invente le premier code pratique permettant
de représenter tout l'alphabet latin (ainsi que des symboles de
ponctuation usuels) sur $5$ bits. Ce code est présenté à la figure ci-dessous.

La machine de \textsc{Baudot} comprenait un clavier de $5$ touches,
avec lequel on entrait les caractères à transmettre, selon le code que
nous avons présenté. Le récepteur disposait d'une machine imprimant
automatiquement les caractères transmis sur un ruban de papier. La
cadence à laquelle les caractères pouvaient être transmis (par
exemple, $10$ caractères par minute) était mesurée en \emph{bauds},
une unité que l'on connait encore aujourd'hui, bien qu'elle soit
tombée en désuétude. Le système de \textsc{Baudot} sera plus tard
amélioré pour utiliser du ruban perforé pour stocker les informations
à transmettre. Cela donnera lieu aux \emph{téléscripteurs} (ou
\textit{teletypes} en anglais, abbrévié TTY), sortes de machines à
écrire qui ont la capacité d'envoyer et de recevoir du texte en
utilisant le code de \textsc{Baudot}, sur des lignes
téléphoniques. Ces systèmes ont été largement utilisés pour la
transmission d'information, notamment par la presse, jusque dans les
années 1980.

\begin{figure}
  \centering

  \includegraphics[width=1.0\textwidth]{images/Chap2/Code-Baudot.png}
    
  \caption[Le code Baudot]{Le code Baudot, un système historique de représentation
    binaire des caractères (on peut associer le $+$ à $1$ et le $-$ à
    $0$), breveté en 1882 en France. On voit ici un extrait du brevet
    américain (1888).}
%  \label{fig:baudot}
\end{figure}

\paragraph{Le code ASCII} L'évolution la plus marquante des
développements historiques que nous venons de présenter est le code
ASCII (\textit{American Standard Code for Information Interchange},
présenté en 1967), qui est encore en usage aujourd'hui. Il comprend
$128$ caractères encodés sur $7$ bits, et est présenté à la figure suivante.
%\figurename~\ref{fig:ascii}.

\begin{figure}
  
  \centering
  \includegraphics[width=1.0\textwidth]{images/Chap2/ascii.png}
  \caption[Le code ASCII]{Le code ASCII. Chaque caractère est
    représenté par une valeur hexadécimale sur 2 chiffres: le chiffre
    de poids fort est donné par la ligne, le chiffre de poids faible
    par la colonne.}
%  \label{fig:ascii}

\end{figure}

\exemple{
  
  Avec le code ASCII la chaîne de caractères \texttt{MOOC NSI} est
  représentée par (en hexadécimal):
  
  \begin{center}
    \texttt{4D 4F 4F 43 20 4E 53 49},
  \end{center}

  ou, en binaire, par:

  \begin{center}
    $100\ 1101\ \ 100\ 1111\ \ 100\ 1111\ \ 100\ 0011\ \ 010\ 0000\ \ 100\ 1110\ \ 101\ 0011\ \ 100\ 1001$  
  \end{center}

}


Ce code, bien adapté à la langue anglaise, ne permet pas de
représenter les caractères accentués. C'est pourquoi plusieurs
extensions ont vu le jour, sur $8$ bits, où les 128 caractères
supplémentaires permettaient d'encoder les caractères propres à une
langue choisie. Par exemple sur l'IBM PC, ces extensions étaient
appelées \textit{code pages}. En voici deux exemples:
\begin{itemize}
\item le \textit{code page} 437: le jeu de caractère ASCII standard auxquels on a
  ajouté les caractères accentués latin;
\item le \textit{code page} 737: le code ASCII standard plus les caractères grecs,
  voir figure suivante. %\figurename~\ref{fig:cp737}.
\end{itemize}
L'utilisation de ces \textit{code pages} supposait que l'utilisateur
connaissaient le code qui avait été utilisé pour représenter le texte
source, et qu'il disposait des moyens logiciels et matériels pour
afficher les caractères correspondants. En pratique, cela se révélait
souvent fastidieux, et donnait lieu à des surprises si on se trompait
de \textit{code page}\ldots

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/Chap1/CP737.png}
  \caption[Le {\textit{Code Page}} 737]{Le {\textit{Code Page}} 737:
    une extension du code ASCII pour obtenir les caractères
    grecs. \cite{wiki:cfp737}}
  %({\tt images/Chap1/CP737.pdf}).
  %\label{fig:cp737}
\end{figure}

% \begin{sidewaysfigure}
%   \centering
%   \includegraphics[width=.8\textwidth]{images/Chap1/CP857.pdf}
%   \caption{Le {\textit{Code Page}} 857. \cite{wiki:cfp857}}
%   % ({\tt images/Chap1/CP857.pdf})
%   \label{fig:cp857}
% \end{sidewaysfigure}

\paragraph{Unicode} Plus récemment, le projet
\textit{Unicode}\footnote{Le consortium qui conçoit et publie Unicode
  possède un site web \url{https://unicode.org/}. } a vu le jour, et
s'est donné pour objectif de créer une norme reprenant la plupart des
systèmes d'écriture utilisés dans le monde, et de leur attribuer un
encodage. La version en cours d'Unicode est la version 11 et elle
comprend $137\ 439$ caractères. La norme Unicode associe plusieurs
encodages à chaque caractère. L'encodage le plus courant est appelé
UTF-8: il s'agit d'un encodage à longueur variable car chaque
caractère est encodé sur 1, 2, 3 ou 4 octets. Tous les caractères
encodés sur 1 octet sont compatibles avec le standard ASCII. Les
encodages sur 2, 3 et 4 octets sont utilisés pour représenter d'autres
caractères, aussi exotiques soient-ils\ldots\ Par exemple, la
figure suivante présente l'encodage Unicode de
l'alphabet Tagbanwa, en usage aux Philippines
\cite{wiki:tagbanwa,tagbanwa}.

La norme Unicode est devenue aujourd'hui bien adoptée,
notamment par les sites et navigateurs web.

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{images/Chap1/tagbanwa.png}
  \caption{Extrait du standard Unicode, alphabet Tagbanwa
    \cite{wiki:tagbanwa}.}
  %\label{fig:unicode}
  %http://www.unicode.org/charts/PDF/U1760.pdf
\end{figure}



