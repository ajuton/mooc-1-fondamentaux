\chapter{Leçon 18 --
  Gestion des processus et de la mémoire secondaire}\label{cha:processus}


\section{Gestion des processus}
Comme nous l'avons dit dans l'introduction, un programme est
l'ensemble des instructions et des données qui permettent de
réaliser un traitement automatisé. Une fois le \emph{programme}
réalisé, il faut l'\emph{exécuter}, c'est-à-dire, faire en sorte que
le processeur exécute les instructions du programme. \`A partir du
moment où un programme est exécuté, il devient un \emph{processus}.

Il est important de faire la différence entre \emph{programme} et
\emph{processus}. En effet, on peut très bien imaginer, sur un système
\emph{multi-tâches}, exécuter plusieurs \emph{copies} du même
programme (pour le faire travailler sur des données différentes dans
un système interactif, par exemple). Dans ce cas, il faut pouvoir
distinguer entre ces différentes versions: ce seront des processus
différents. Par ailleurs, un processus est composé du programme qui
s'exécute, mais aussi d'un ensemble d'informations qui spécifient
l'avancement de l'exécution et qu'on appelle le \emph{contexte}. Ce
\emph{contexte} doit contenir au moins les informations suivantes:
\begin{itemize}
\item La zone mémoire qui appartient au processus.
\item Le compteur de programme, qui indique la prochaine instruction à
  exécuter. Quand le processus est occupé à s'exécuter, c'est la
  valeur qui se trouve dans le registre \verb-PC-.
\item Les contenus des différents registres de travail.
\item Le \emph{statut} du processus (\textit{cfr. infra})
\item Un pointeur vers un \textit{stack}.
\end{itemize}
Par ailleurs, si le système possède une mémoire paginée, il faudra
également associer au processus une table des pages. Même si c'est le
même programme qui est exécuté dans deux processus différents, on ne
peut pas (\textit{a priori}) partager leurs pages en mémoire, car il
peut arriver que les processus y écrivent des informations
différentes.

Naturellement, la gestion des processus sera une tâche confiée à
l'OS. Il reste deux questions à examiner:
\begin{enumerate}
\item Quel est le cycle de vie d'un processus ?
\item Comment peut-on exécuter plus de processus qu'il n'y a de CPU
  disponibles ?

\end{enumerate}

\subsection{Cycle de vie d'un processus}
\begin{enumerate}
\item \textbf{Création}: sur la plupart des OS, un processus ne peut
  être créé que par l'OS, et uniquement à la demande d'un autre
  processus. Cette demande se fait à l'aide d'un appel système. Après
  l'appel système, il y a un processus de plus sur le système. Le
  processus qui a fait la demande de création est appelé
  \emph{processus père}, et le nouveau processus est appelé
  \emph{processus fils}.

  Lors de la création, l'OS doit essentiellement créer un
  \emph{nouveau contexte} pour le nouveau processus, et retenir dans ses
  tables les caractéristiques de ce nouveau processus.

  Comme un processus ne peut être créé que s'il en existe déjà un
  autre, il faut qu'un premier processus sont créé au démarrage du
  système, de manière automatique. Sur les systèmes Unix, par exemple,
  c'est le processus \verb-init-. Celui-ci, une fois créé, lit un
  fichier de configuration sur le disque, qui lui indique ce qu'il y a
  lieu de faire. En général, cela consiste à d'abord exécuter une
  série d'autres processus en \textit{batch} pour finir d'initialiser
  le système (configuration de périphériques, lancement de processus
  d'arrière-plan, \textit{etc}). Ensuite, \verb-init- crée un
  processus dont le rôle est d'attendre les commandes de
  l'utilisateur, de les interpréter (à l'aide de l'interpréteur de
  commande si nécessaire) et de les exécuter. Cela peut être une
  \emph{invite de commande} où l'utilisateur entre des commandes au
  clavier de manière textuelle, ou bien un panneau comprenant des
  boutons sur lesquels on clique pour lancer des applications (barre
  des tâches, \textit{dock},\ldots)
\item \textbf{Actif}: Lorsque le processus s'exécute sur un des CPU,
  on dit qu'il est actif.
\item \textbf{En attente}: Lorsque le processus n'a pas fini de
  s'exécuter, qu'il désire disposer du CPU, mais qu'il n'en dispose
  pas, il est en attente.
\item \textbf{Bloqué}: Lorsque le processus n'a pas fini de
  s'exécuter, mais qu'il n'est pas capable de continuer à s'exécuter
  (par exemple parce qu'il attend un périphérique), le processus est
  bloqué.
\item \textbf{Terminé}: Lorsque le processus a terminé son exécution,
  il le signale à l'OS à l'aide d'un appel système. L'OS doit alors
  libérer les dernières ressources encore utilisées par le processus
  (mémoire, fichiers encore ouverts,\ldots). Un processus peut aussi
  être \emph{tué} par l'OS: quand le processus commet une erreur
  (\textit{trap}, accès à une zone mémoire interdite détectée lors
  d'un \textit{page fault}), l'OS peut décider de le faire passer à
  l'état terminé. On risque alors de perdre des données.

  \textbf{Rem}: sur les systèmes Unix, un processus peut également
  devenir un \emph{zombie} après être terminé. Cela arrive quand il
  doit encore renvoyer une valeur à son parent, et que celui-ci ne l'a
  pas encore lue. Le processus fils est donc bien mort, mais il occupe
  encore de la place dans les tables de l'OS, comme un processus
  vivant, pour stocker la valeur de retour. C'est donc bien un
  mort-vivant\ldots
\end{enumerate}
Un processus peut passer de l'état \emph{actif} à l'état \emph{en
  attente} de manière volontaire ou non: soit en signalant, à l'aide
d'un appel système, à l'OS qu'il ne souhaite plus disposer du CPU
temporairement (pour attendre un périphérique, par exemple); soit
parce qu'il a épuisé son \textit{quantum} de temps (\textit{cfr. time
  sharing} ci-dessous).

Le passage de l'état \emph{actif} à l'état \emph{bloqué} dépend de la
situation du processus et des périphériques. Une fois que le processus
est <<~débloqué~>> (c'est-à-dire que la condition qui le bloquait est
levée), le processus passe en \emph{attente} pour espérer obtenir le
CPU.

Par contre, c'est toujours l'OS qui décide quel processus, parmi ceux
en attente, va devenir actif. Comme on l'a déjà vu, une partie de
l'OS, appelée \emph{ordonnanceur} est en charge de cette
décision. Cette décision sera prise en fonction de plusieurs critères
(par exemple si le système est temps réel).

Le changement de processus actif sur un CPU est donc toujours réalisé
par l'OS. Pour ce faire, l'OS doit essentiellement réaliser un
\emph{changement de contexte} qui consiste à remplacer le contexte de
l'ancien processus par le contexte du nouveau afin que celui-ci puisse
continuer à s'exécuter correctement. Cela peut naturellement prendre
du temps, surtout si le nouveau processus a ses pages présentes en
mémoire secondaire et qu'il faut les recharger par exemple.

\subsection{Systèmes en \textit{Time sharing}}
La technique du \textit{time sharing} (partage de temps) permet à l'OS
de faire exécuter plus de processus qu'il n'y a de CPU disponibles,
tout en donnant l'illusion pour l'utilisateur que ces processus
s'exécutent véritablement \emph{en parallèle}, c'est-à-dire, comme
s'ils s'exécutaient chacun sur un CPU dédié.

Dans ce que nous avons expliqué ci-dessus sur les états des processus,
on a vu qu'un processus passait la plus grande partie de sa vie entre
les états \emph{actifs}, \emph{bloqué} et \emph{en attente}. Le
passage d'\emph{actif} à \emph{bloqué} et de \emph{bloqué} à \emph{en
  attente} ne dépend pas de l'OS, en général. Par contre, l'OS doit
décider quand passer d'\emph{actif} à \emph{en attente} et vice-versa.

Afin de répartir une utilisation \emph{équitable} du (des) CPU(s)
entre les processus, le système de \textit{time sharing} consiste à
définir un intervalle de temps très court, appelé \emph{quantum de
  temps}, durant lequel un processus a le droit de s'exécuter. À la
fin de cet intervalle, le processus doit libérer le CPU afin qu'un
autre processus puisse s'exécuter pour le même \emph{quantum} de
temps, et ainsi de suite. L'ordonnanceur du système décide quel est le
processus suivant (il peut procéder de manière cyclique, ou bien
utiliser une système de priorités, par exemple).

Comment peut-on faire en sorte qu'une fois le quantum de temps épuisé,
le processus <<~rende la main~>> ? Il y a deux techniques:
\begin{itemize}
\item \textit{Time sharing} coopératif: le processus consulte
  régulièrement l'horloge système et rend la main dès qu'il a épuisé
  son \textit{quantum} de temps. Le bon fonctionnement du système
  repose donc sur la bonne volonté des programmeurs des programmes
  actifs, et sur le fait que ceux-ci ne plantent pas en bloquant tout
  le système\ldots Cette solution a été utilisée dans les anciennes
  versions de MacOS et de Windows, par exemple. Ces techniques ont
  l'avantage d'être hautement prévisibles, si on connaît bien les
  programmes exécutés, et sont donc encore souvent utilisés dans le
  cadre des systèmes embarqués.
\item \textit{Time sharing} préemptif: l'horloge système est
  programmée pour déclencher, à intervalle réguliers (séparés par le
  \textit{quantum de temps}), une interruption. Le processus actif
  sera donc interrompu \textit{volens nolens}, et l'OS effectuera le
  changement de contexte approprié.
\end{itemize}

En cas de surcharge du système (trop de processus), les changements de
contexte peuvent devenir très fréquents. Si les processus présents
consomment beaucoup de mémoire, il y a un risque que chaque changement
de contexte entraîne le chargement d'une page mémoire depuis la
mémoire secondaire. Les changements de contexte prennent alors
énormément de temps, au pire cas, plus de temps que l'exécution des
processus eux-mêmes. On a alors un phénomène de \textit{trashing}.


\section{Entrées/sorties virtuelles et systèmes de fichiers}
L'organisation de la mémoire secondaire pose \textit{grosso modo} les
mêmes problèmes que celle de la mémoire primaire:
\begin{enumerate}
\item Les processus utilisateurs ne peuvent pas avoir accès
  \emph{directement} à la mémoire secondaire, et ne peuvent donc pas
  décider de lire ou écrire à une adresse précise.
\item Les données appartenant aux différents utilisateurs doivent être
  bien séparées et organisées. Un utilisateur ne peut pas avoir la
  possibilité d'accéder aux données d'un autre utilisateur sans que ce
  dernier ne lui ait donné la permission. Il faut aussi protéger les
  données contre les erreurs, effacement involontaires, \emph{etc}.
\item Pour simplifier le travail des programmes utilisateur, les
  données doivent être organisées de manière linéaire, comme une
  séquence d'octets. Concrètement, cela n'est pas toujours possible,
  car les espaces disponibles ne sont pas toujours assez grands.
\end{enumerate}

\subsection{Fichiers}
Pour résoudre ces problèmes, l'OS présente les données sous la forme
d'une abstraction appelée <<~fichier~>>. Un fichier est simplement
une séquence d'octets, à laquelle les programmes utilisateurs peuvent
accéder en utilisant des appels systèmes. Concrètement, quand un
processus veut lire ou écrire en mémoire secondaire:
\begin{enumerate}
\item Il doit identifier un fichier dans lequel lire ou écrire. Le cas
  échéant, il doit demander à l'OS d'en créer un.
\item Il doit demander à l'OS d'\emph{ouvrir} ce fichier, c'est-à-dire
  obtenir un accès à ce fichier.
\item Il doit faire appel à l'OS pour lire ou écrire les données de
  manière séquentielle: d'abord la première, puis la seconde,
  \textit{etc}. Il est aussi parfois possible de revenir en arrière,
  ou de faire une <<~avance rapide~>>.
\item Il doit finalement \emph{fermer} le fichier grâce à un appel
  système, c'est-à-dire relâcher l'accès qu'il possède à ce fichier.
\end{enumerate}

Comme on le voit, le programme utilisateur voit le fichier comme une
séquence d'octets qui ont chacun une adresse, qui est leur décalage
par rapport au début du fichier. Ainsi, dans chaque fichier, la
première information se trouve au décalage 0. Ces décalages ne sont
évidemment pas les adresses sur le disque: l'OS doit se charger
d'effectuer une \emph{traduction} d'adresse, un peu comme le MMU dans
le cas de la mémoire primaire. On a donc à nouveau affaire à des
adresses \emph{virtuelles} qui doivent être traduites en adresse
\emph{réelles}.

Concrètement, la mémoire secondaire est divisée en \emph{blocs} dont
la taille peut varier en fonction du type de mémoire. Chaque bloc est
numéroté, comme les cases mémoire en mémoire secondaire. Afin de
pouvoir effectuer la traduction des adresses, l'OS maintient plusieurs
tables:
\begin{enumerate}
\item Une \emph{table d'allocation des fichiers} (\textit{File
    Alocation Table}): qui reprend la liste de tous les fichiers
  présents sur le système, et des informations annexes (permissions,
  possesseur, date de création, \textit{etc}).
\item Pour chaque fichier (donc chaque entrée de la table
  d'allocation): une liste des blocs qui constituent le fichier.
\end{enumerate}

Par ailleurs, pendant l'exécution d'un processus, l'OS lui associe une
table qui collecte tous les fichiers ouverts par ce processus. Pour
chaque fichier ouvert, on trouve un \emph{descripteur de fichier} qui
contient plusieurs informations, notamment:
\begin{enumerate}
\item L'identification du fichier, par exemple, son numéro d'entrée
  dans la table des fichiers.
\item Des informations annexes: on peut avoir la permission de lire
  mais pas d'écrire par exemple\ldots
\item Un pointeur vers un \emph{cache} (ou \textit{buffer) }en mémoire
  où copier/lire les données à lire/écrire dans le fichier.
\item Un pointeur indiquant le décalage courant dans le fichier.
\end{enumerate}

En cas de lecture, on a généralement un comportement qui consiste à:
\begin{enumerate}
\item Copier le bloc à l'adresse virtuelle donnée par le décalage du
  descripteur de fichier dans le \textit{cache}.
\item Incrémenter ce pointeur.
\end{enumerate}

En cas d'écriture, la situation est un peu plus délicate si on désire
faire une \emph{insertion} (c'est-à-dire ne pas écraser les
informations courantes, ou allonger le fichier). Dans ce cas, le
fichier va devoir recevoir un ou plusieurs \emph{nouveaux blocs}, que
l'OS va lui attribuer. Pour ce faire l'OS doit également maintenir une
\emph{liste des blocs libres}. En cas d'allongement d'un fichier l'OS
doit:
\begin{enumerate}
\item Supprimer un bloc de la liste des blocs libres.
\item L'insérer au bon endroit dans la liste des blocs du fichier.
\end{enumerate}

On procède de manière inverse en cas de libération de bloc.

\paragraph{Problèmes liés à la politique par blocs: fragmentation} Un
des risques liés à ce système est de voir les blocs qui constituent un
même fichier se retrouver éparpillés (de manière non-contiguë) sur la
mémoire secondaire. Cela peut poser problème pour les lecteurs de
disque dur ou de CDROMs par exemple, car cela entraîne un déplacement
continu de la tête de lecture, ce qui prend beaucoup de temps. On peut
implémenter dans l'OS des politiques d'attribution des blocs qui
évitent ces problème. Sur certains OS, il existe par contre un
programme dédié qui se charge de replacer correctement les blocs sur
le disque pour diminuer la fragmentation, mais cela prend beaucoup de
temps\ldots

\subsection{Répertoires}
Afin de pouvoir mieux organiser l'information, on peut répartir
(virtuellement toujours) les fichiers dans différents
\emph{répertoires}. Un répertoire est une sorte de conteneur pour:
\begin{enumerate}
\item d'autres répertoires
\item des fichiers
\end{enumerate}
Comme les fichiers, les répertoires portent un nom et ont des
permissions d'accès. Comme un répertoire peut contenir d'autres
répertoires, l'organisation de la mémoire secondaire devient une
\emph{hiérarchie} avec un \emph{répertoire principal} (appelé
<<~racine~>>) et des sous-répertoires. Concrètement:
\begin{enumerate}
\item La structure en répertoires doit apparaître dans la table
  d'allocation, qui a maintenant une structure \emph{arborescente}.
\item Le \emph{nom} d'un fichier doit maintenant être complété par le
  nom du (des) répertoire(s) qui le contient
  (contiennent). L'affichage de ce nom à destination de l'utilisateur
  utilise certaines conventions qui dépendent de l'OS. Par exemple
  \verb-/rep1/rep2/fichier- sous Unix, \verb-\rep1\rep2\fichier- sous
  Windows ou encore \verb-rep1:rep2:fichier- sous MacOS.
\end{enumerate}

L'ensemble des répertoires et des fichiers ainsi organisés est appelé
un \emph{système de fichiers}. %Voir illustration à la Fig. 6--36.

\subsection{Quelques remarques}
\begin{enumerate}
\item Beaucoup de périphériques se comportent, à un certain niveau
  d'abstraction, comme des fichiers, c'est-à-dire comme des séquences
  d'octets. Par exemple, le clavier émet une séquence d'octets
  représentant les touches enfoncées, qu'on peut lire comme on lit un
  fichier. De même, pour commander une imprimante, on peut lui envoyer
  une séquence d'octets comme si on écrivait dans un fichier.  C'est
  également le cas d'autres services de l'OS, par exemple la
  génération de nombres aléatoires.

  C'est pourquoi, sur plusieurs OS, l'accès à ces périphériques est
  possible à travers des \emph{fichiers virtuels}: ce sont des
  fichiers qui ne sont pas physiquement présents sur le disque, mais
  qui apparaissent quand même dans la table d'allocation des fichiers
  avec un \textit{flag} spécial qui indique le statut
  particulier. Quand on y accède l'OS les traite de façon
  différente. Par exemple:
  \begin{itemize}
  \item Si on lit un tel fichier qui correspond à un périphérique,
    l'OS va lire les données fournies par le périphérique pour
    alimenter le cache. \`A aucun moment les données lues ne seront
    stockées sur le disque.
  \item Si on lit \verb-/dev/random- sur un système Unix, l'OS génère
    des nombres aléatoires pour alimenter le cache mais ne les stocke
    pas sur le disque non plus. Pour avoir des zéros, on peut utiliser
    \verb-/dev/zero-.
  \item Certains de ces fichiers spéciaux permettent l'écriture au
    lieu de la lecture: par exemple \verb-/dev/null- qui ne fait\ldots
    rien avec les données qu'on y écrit.
  \end{itemize}
\item Comme le système de fichiers est une abstraction de la mémoire
  secondaire, on peut aisément ajouter des \emph{services} utiles à
  l'utilisateur:
  \begin{itemize}
  \item La possibilité d'avoir des \emph{liens symboliques},
    c'est-à-dire des fichiers \emph{virtuels} qui sont en fait des
    pointeurs vers d'autres fichiers. Quand on ouvre le lien virtuel,
    on ouvre en fait le fichier vers lequel il pointe, qui peut se
    trouver à un endroit différent du système de fichiers. Cela évite
    de devoir maintenir plusieurs copies du même fichier à des
    endroits différents dus système. \`A nouveau, ce type de fichier
    est identifié par un \textit{flag} spécial dans la table
    d'allocation.
  \item La possibilité d'avoir un \emph{indexage} automatique du
    contenu des fichiers (l'index étant stocké dans les blocs libres
    du système de fichiers), comme le système \emph{Spotlight}
    d'Apple.
  \item La possibilité d'avoir des systèmes tolérants aux pannes, qui
    sont capables de remettre le système dans un état cohérent en cas
    de panne durant une écriture qui n'aurait pas été complètement
    terminée. C'est le cas sur les systèmes Linux avec le système de
    fichiers \verb-Ext3-.
  \end{itemize}
\item Le cas où plusieurs périphériques de mémoire secondaire sont
  présents sur le système est géré de manière différente par
  différents OS:
  \begin{itemize}
  \item Sur les OS <<~à la Windows~>> : chaque périphérique porte un
    nom différent, en général une lettre. Le nom complet d'un fichier
    commence donc par cette lettre:
    \verb-C:\Windows\...\fichier-. Chaque périphérique possède donc
    son propre système de fichiers.
  \item Sur les OS <<~à la Unix~>> : un périphérique principal contient
    le système de fichiers \emph{racine}, c'est-à-dire celui qui
    contient le répertoire racine. Les autres périphériques
    apparaissent comme des sous-répertoires (pas nécessairement
    directs) de ce système (on parle de \emph{monter} un
    périphérique). Ainsi, tous les périphériques apparaissent dans un
    seul système de fichiers. Ce système permet également d'avoir des
    répertoires virtuels\ldots
  \end{itemize}
\end{enumerate}

\begin{center}
  \aldineleft\vspace*{1cm} \decoone\vspace*{1cm} \aldineright
\end{center}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
