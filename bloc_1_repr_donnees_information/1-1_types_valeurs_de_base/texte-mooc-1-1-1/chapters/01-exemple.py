"""
Calcul des racines d'une équation du second degré
"""
__author__="Thierry Massart"
__date__="22 août 2012"

from math import sqrt
a = float(input("valeur de a : "))
b = float(input("valeur de b : "))
c = float(input("valeur de c : "))

delta = b**2 - 4*a*c
if delta < 0:
   print(" pas de racines réelles")
elif delta == 0:
   print("une racine : x = ", -b/(2*a))
else:
   racine = sqrt(delta) 
   print("deux racines : x1 = ", \
      (-b + racine)/(2*a), " x2 = ", (-b - racine) / (2*a))
