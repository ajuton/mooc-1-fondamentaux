# Sommaire B1-M1

## 1-1 Présentation du module : Objectifs du module

## 1-1-1 Représentation binaire de l'information

## 1-1-2 Unité de quantité d'information

## 1-1-3 Écriture des nombres dans différentes bases
- 1-1-3-1 Introduction : la notion de base
- 1-1-3-2 Changement de base
- 1-1-3-3 Manipuler les données en binaire
- 1-1-3-4 Nombres signés
- 1-1-3-5 Nombres avec partie fractionnaire

## 1-1-4 Représentation du texte

## 1-1-5 Représentation d'images, de sons, de vidéos

## 1-1-6 Exercices


