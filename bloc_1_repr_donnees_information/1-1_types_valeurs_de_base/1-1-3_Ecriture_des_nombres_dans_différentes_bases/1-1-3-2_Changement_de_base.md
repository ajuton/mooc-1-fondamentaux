# Écriture des nombres dans différentes bases 2/5 : Changements de base

1. Introduction : la notion de base
2. **Changements de base** (2 vidéos)
3. Manipuler les données en binaire
4. Nombres signés
5. Nombres avec partie fractionnaire

[![Vidéo 2 part1 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S4.mp4)

## Transcription de la vidéo 

(en cours de mise en place)

[![Vidéo 2 part2 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S5.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
