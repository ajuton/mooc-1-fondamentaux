# Écriture des nombres dans différentes bases 4/5 : Nombres signés

1. Introduction : la notion de base
2. Changements de base
3. Manipuler les données en binaire
4. **Nombres signés**
5. Nombres avec partie fractionnaire

[![Vidéo 4 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S7.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S7.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
