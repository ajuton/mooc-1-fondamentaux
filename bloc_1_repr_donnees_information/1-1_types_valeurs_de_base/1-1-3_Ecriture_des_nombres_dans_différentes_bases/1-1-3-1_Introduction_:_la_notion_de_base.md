# Écriture des nombres dans différentes bases 1/5 : Introduction,la notion de base

1. **Introduction : la notion de base**
2. Changements de base
3. Manipuler les données en binaire
4. Nombres signés
5. Nombres avec partie fractionnaire

[![Vidéo 1 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S3.mp4)

## Transcription de la vidéo 

(en cours de mise en place)
