# Sommaire B2-M3

## 2-3 Présentation du module

## 2-3-1 Efficacité d’un algorithme et d’un programme

## 2-3-2 Approche pragmatique

## 2-3-3 Approche analytique

## 2-3-4 La notion du grand O

## 2-3-5 Le calcul du grand O

## 2-3-6 Application des règles de calcul du grand O

## 2-3-7 Complexité des tris simples

## 2-3-8 Complexité des manipulations des séquences de données python
