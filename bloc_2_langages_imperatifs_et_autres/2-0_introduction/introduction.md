# Langages et programmation

Ce bloc couvre principalement les aspects langages de programmation.

## Prérequis

Tout d'abord nous supposons que les **bases de la programmation impérative Python** vous sont acquises.  La matière correspond au contenu du MOOC ["Apprendre à coder avec Python"](https://www.fun-mooc.fr/fr/cours/apprendre-a-coder-avec-python/).

Son contenu est le suivant :

- Installation d'un environnement Python sur votre ordinateur
<!-- - Présentation des outils UpyLaB et Python Tutor -->
- Quelques définitions de base
- Valeurs et expressions arithmétiques et textuelles
- Les variables
- La console et les scripts
- Quelques fonctions prédéfinies
- Les modules math et turtle
- Mon premier programme complet
- L'instruction conditionnelle if
- Les instructions répétitives while et for
- L'instruction pass
- Les fonctions prédéfinies et définies
- Quelques règles de bonnes pratiques de programmation
- Manipulations de base des séquences
- Découpage (slicing) et opérations de manipulation des séquences
- Méthodes de manipulation des séquences
- Compréhension et copies de listes
- Les fichiers textes et les matrices
- Manipulations de base des séquences ensembles et dictionnaires
- Méthodes de manipulation des dictionnaires
- Dictionnaires persistants

Comme son nom l'indique, le but principal du MOOC est d'**apprendre à coder**, cela en utilisant Python comme langage pour supporter cet apprentissage, et nullement de connaître le langage Python de façon avancée ; il nous paraît en effet essentiel pour un enseignant d'être à l'aise dans la programmation plus que d'être un expert du langage Python. Il est pourtant clair, qu'à l'issue de cet apprentissage, le noyau impératif de Python a été suffisemment abordé pour pouvoir enseigner la programmation Python en NSI (excepté la partie orienté-objet).

Par contre, nous laissons les autres modules de ce présent cours et le MOOC "Apprendre à enseigner le NSI" ([MOOC NSI 2ème partie "pratique"](https://lms.fun-mooc.fr/courses/course-v1:inria+41027+session01/courseware/c3fff45a699d4d3cb498c9d2fba5a9aa/)), le soin d'aller plus loin dans les aspects : mise au point d'un programme, prototypage, description des pré et post-conditions, test et documentation d'un code, utilisation d'un API et création de modules.  Les aspects récursivités sont abordés dans le chapitre algorithmique de ce présent cours.

## Aspects OO de Python

Dans le module suivant, nous regardons les **aspects orientés objets de Python**. Nous présentons le vocabulaire de la POO, la syntaxe Python pour les bases et quelques concepts plus avancés (comment réaliser l'encapsulation par exemple). Nous poursuivons le module par quelques vidéos pratiques sur un mini-projet réalisé en objet avec Python et son module `turtle`.

Après cette _mise en bouche_ pragmatique, nous abordons les choses de façon plus générique.

## Histoire et paradigmes des langages de programmation

Dans le module 2 de ce bloc 2, nous prenons de la distance avec Python, et nous nous concentrons sur les **paradigmes de programmation** auquel nous ajoutons des éléments historiques par exemple sur la génèse de différents langages de programmation (Un résumé plus séquentiel peut être trouvé dans [Histoire des langages de programmation sur Wikipedia](https://fr.wikipedia.org/wiki/Histoire_des_langages_de_programmation)).

Nous regardons d'abord le fonctionnement des langages impératifs y compris une brève explication du fonctionnement des interpréteurs et compilateurs, ainsi que sur la comparaison, en particulier sur base d'un petit exemple de programmation, des langages impératifs "populaires".

Nous analysons ensuite les langages orientés objet et les concepts qui y sont développés.

Nous concluons ce module et le Bloc par une brève discussion sur les paradigmes de programmation (en particulier, des langages impératifs et fonctionnels) en ajoutants quelques repères historiques sur certains des langages de programmation phares existants.

> Notons que le [Concours externe de l’agrégation du second degré - Section informatique - Programme de la session 2022](https://media.devenirenseignant.gouv.fr/file/agregation_externe_21/22/3/p2022_agreg_ext_informatique_1414223.pdf), que nous ne prétendons évidemment pas couvrir  avec nos MOOCs, suppose la connaissance de Python, C et  OCaml (ainsi que SQL en matière de langage de requêtes).
